{
  inputs = {
    m100-utils-flake.url = "gitlab:trs-80-model-100/utils";
  };
  outputs = { self, nixpkgs, flake-utils, m100-utils-flake }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        m100-utils = m100-utils-flake.packages.${system}.default;
      in
      {
        devShells.default = with pkgs; mkShell {
          packages = [
          ];

          buildInputs = [
            m100-utils
          ];

          M100_UTILS_MK = "${m100-utils}/make";
        };
      }
    );
}

